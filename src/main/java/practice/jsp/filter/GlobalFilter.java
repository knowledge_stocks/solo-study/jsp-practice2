package practice.jsp.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter("/*")
public class GlobalFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request,
			ServletResponse response,
			FilterChain chain)
			throws IOException, ServletException {
		// chain을 하기 전에 request의 내용을 UTF-8로 인코딩 처리
		// 필터에 걸리는 모든 req를 UTF-8로 처리할 수 있다.
		request.setCharacterEncoding("UTF-8");
		
		// chain.doFiler를 통해 request와 response를 원래의 타겟에게 전달한다
		// 이를 전달하면 사용자에게 원래의 결과가 출력된다.
		chain.doFilter(request, response);
	}

}