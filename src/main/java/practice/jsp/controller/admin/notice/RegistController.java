package practice.jsp.controller.admin.notice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import practice.jsp.model.Notice;
import practice.jsp.service.NoticeService;

@MultipartConfig(
		// location = "C:\\tmp", // location은 절대경로를 입력해야하기 때문에 운영체제 마다 경로가 갈리기 때문에 일반적으로 설정하지 않는다고 한다.
		// location을 생략하면 ""로 설정되며, 자바에서 지정하는 임시 경로가 설정된다고 한다.
		fileSizeThreshold = 1024*1024, // 1MB보다 작으면 메모리에, 보다 크면 location에 지정한 경로에 파일로
		maxFileSize = 1024*1024*50, // 파일 하나하나의 최대 사이즈, 각 파일은 50MB를 넘을 수 없다.
		maxRequestSize = 1024*1024*50*5 // 전체 리퀘스트의 최대 사이즈, 모든 파일의 합은 250MB를 넘을 수 없다.
)
@WebServlet("/admin/board/notice/reg")
public class RegistController extends HttpServlet {
	private static final long serialVersionUID = -6561328858743894216L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/admin/board/notice/reg.jsp");
		dispatcher.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String title = req.getParameter("title");
		String content = req.getParameter("content");
		String open = req.getParameter("open");
		
		boolean pub = false;
		if(open != null) {
			pub = true;
		}

		Collection<Part> parts = req.getParts();
		StringBuilder filesBuilder = new StringBuilder();
		for(Part part : parts) {
			if(!part.getName().equals("file")
				|| part.getSize() <= 0) {
				continue;
			}
			
			// 파일 이름, 확장자 구하기
			String fileName = part.getSubmittedFileName();
			String fileExtension = "";
			int dotIdx = -1;
			if((dotIdx = fileName.lastIndexOf('.')) != -1) {
				fileExtension = fileName.substring(dotIdx);
				fileName = fileName.substring(0, dotIdx);
			}
			
			// 중복 파일 이름 충돌 방지
			// 웹 Root 밑의 upload 폴더의 절대 경로 가져오기
			String uploadPath = req.getServletContext().getRealPath("/upload");
			File uploadDir = new File(uploadPath);
			if(!uploadDir.exists()) {
				uploadDir.mkdirs();
			}
			
			File uploadFile = new File(uploadPath, fileName + fileExtension);
			for(int num = 1; uploadFile.exists(); num++) {
				uploadFile = new File(uploadPath, fileName + "(" + num + ")" + fileExtension);
			}
			
			// 파일 업로드
			InputStream is = part.getInputStream();
			FileOutputStream fos = new FileOutputStream(uploadFile);
			
			byte[] buf = new byte[1024]; // 1KB씩 작업하기 위함
			int size = 0;
			while((size = is.read(buf)) != -1) {
				fos.write(buf, 0, size);
			}
			
			fos.close();
			is.close();
			
			filesBuilder.append(uploadFile.getName() + ",");
		}
		String uploadFiles = null;
		if(filesBuilder.length() > 0) {
			filesBuilder.delete(filesBuilder.length() - 1, filesBuilder.length()); // 맨 뒤의 ,를 제거
			uploadFiles = filesBuilder.toString();
		}
		
		Notice notice = new Notice(0, title, "god", content, null, 0, uploadFiles, pub);
		
		NoticeService service = NoticeService.getInstance();
		service.createNotice(notice);
		
		resp.sendRedirect("list");
	}
}