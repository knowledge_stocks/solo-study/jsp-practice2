package practice.jsp.controller.admin.notice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import practice.jsp.model.NoticeCommentCountView;
import practice.jsp.service.NoticeService;

@WebServlet("/admin/board/notice/list")
public class ListController extends HttpServlet {
	private static final long serialVersionUID = -7592151947208444135L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int page = 1;
		String field = req.getParameter("f");
		String query = req.getParameter("q");
		
		try {
			page = Integer.parseInt(req.getParameter("p"));
		} catch (Exception ignore) {}
		
		NoticeService service = NoticeService.getInstance();
		
		List<NoticeCommentCountView> notices = service.getNoticeList(page, field, query);
		int noticeCount = service.getNoticeCount(field, query);
		
		req.setAttribute("notices", notices);
		req.setAttribute("count", noticeCount);
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/admin/board/notice/list.jsp");
		dispatcher.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] openIds = req.getParameterValues("open-id");
		String[] delIds = req.getParameterValues("del-id");
		String cmd = req.getParameter("notice-cmd");
		String[] allIds = req.getParameter("ids").trim().split(" ");
		
		NoticeService service = NoticeService.getInstance();
		
		switch(cmd) {
			case "일괄공개":
			{
				List<String> oepnIdList = openIds == null
						? new ArrayList<String>()
						: Arrays.asList(openIds);
				List<String> closeIdList = new ArrayList<String>(Arrays.asList(allIds));
				closeIdList.removeAll(oepnIdList);

				service.pubNoticeAll(oepnIdList, closeIdList);

				break;
			}
			case "일괄삭제":
			{
				int[] ids = Arrays.stream(delIds)
						.mapToInt(Integer::parseInt).toArray();
				
				int result = service.removeNoticeAll(ids);
				
				System.out.printf("공지가 %d개 삭제되었습니다.", result);
				
				break;
			}
		}
		
		resp.sendRedirect("list");
	}
}
