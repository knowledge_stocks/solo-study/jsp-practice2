package practice.jsp.controller.notice;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import practice.jsp.model.Notice;
import practice.jsp.service.NoticeService;

@WebServlet("/notice/detail")
public class DetailController extends HttpServlet {
	private static final long serialVersionUID = -6991807844201297267L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		NoticeService service = NoticeService.getInstance();
		
		Notice notice = service.getPubNotice(Integer.parseInt(req.getParameter("id")));
		
		req.setAttribute("notice", notice);
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/notice/detail.jsp");
		dispatcher.forward(req, resp);
	}
}
