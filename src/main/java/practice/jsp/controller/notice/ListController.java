package practice.jsp.controller.notice;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import practice.jsp.model.NoticeCommentCountView;
import practice.jsp.service.NoticeService;

@WebServlet("/notice/list")
public class ListController extends HttpServlet {
	private static final long serialVersionUID = 8552580024956967164L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int page = 1;
		String field = req.getParameter("f");
		String query = req.getParameter("q");
		
		try {
			page = Integer.parseInt(req.getParameter("p"));
		} catch (Exception ignore) {}
		
		NoticeService service = NoticeService.getInstance();
		
		List<NoticeCommentCountView> notices = service.getPubNoticeList(page, field, query);
		int noticeCount = service.getPubNoticeCount(field, query);
		
		req.setAttribute("notices", notices);
		req.setAttribute("count", noticeCount);
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/notice/list.jsp");
		dispatcher.forward(req, resp);
	}
}
