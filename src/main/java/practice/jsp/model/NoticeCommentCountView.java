package practice.jsp.model;

import java.sql.Timestamp;

public class NoticeCommentCountView extends Notice {
	private int commentCount;
	
	public NoticeCommentCountView(int id, String title, String writerId, Timestamp regDate, int hit,
			String files, boolean pub, int commentCount) {
		super(id, title, writerId, null, regDate, hit, files, pub);
		this.commentCount = commentCount;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
}
